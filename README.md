# AFNP connect

## Brève description :

Accéder aux outils développés par l'association partout où vous allez.
Comprendre les neuropathies inflammatoires/auto-immunes, trouver un centre expert, accéder à la médiathèque (vidéos, brochures...), prendre contact avec l'AFNP, accéder à nos plateformes collaboratives...

## Description complète :

AFNP connect s'adresse à toute personne intéressée par les neuropathies autoimmunes et par les outils développés par l'association pour mieux comprendre et agir sur ces maladies rares.

AFNP connect, est une application au service des malades et de leurs proches pour leur permettre d'accèder à tous les services proposés par l'association. Rester informé sur l'actualité, trouver un centre expert proche de chez soi, soutenir la recherche et les actions de l'AFNP, mieux vivre avec la maladie, connaître et comprendre les traitements, appréhender les démarches et mieux connaître ses droits...

AFNP connect est une application développée par l'Association Française contre les Neuropathies Périphériques, association à but non lucratif reconnue d'intérêt général.

## Notes de versions

### Version 0.4

- L'emplacement des images des vidéos qui était laissé blanc hors ligne sont maintenant supprimé lorsque l'application est lancée hors ligne,
- Sous parties plus claires pour les outils,
- Ajouts d'informations et amélioration de l'accessibilité (notamment à travers un meilleur contraste des couleurs),
- Correction du logo dans le play store

### Version 0.3

- Ajout de 3 pages qui reprennent les informations du site de l'AFNP et permettent donc un visionnage hors ligne de ces informations.
- Modification de la page à propos avec ajout des droits cédés par l'AFNP
- Moins de permissions demandées par l'application (voir le rapport [exodus](https://reports.exodus-privacy.eu.org/fr/reports/neuroquali.org/latest/))
- Réparation d'un bug de lien mort dans la version iOS

## Fabriqué avec le logiciel [quasar](https://quasar.dev/)

#### Install the dependencies

```bash
yarn
# or
npm install
```

#### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

#### Build the app for production

```bash
quasar build
```

#### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).
