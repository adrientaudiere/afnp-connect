const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      { path: "a-propos", component: () => import("pages/a-propos.vue") },
      { path: "les-outils", component: () => import("pages/les-outils.vue") },
      {
        path: "traitements",
        component: () => import("src/pages/traitements-info.vue"),
      },
      { path: "vivreavec", component: () => import("src/pages/vivreAvec.vue") },
      {
        path: "neuropathies",
        component: () => import("src/pages/neuropathies-info.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
